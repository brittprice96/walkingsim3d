using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Opendoor : MonoBehaviour
{
 public Text Msg;
   public GameObject Door;
   public AudioClip DoorOpen;
    void Start()
    {
        
    }
private void OnCollisionEnter(Collision other) {
        if (other.gameObject.name == "Player")
    {
    Msg.gameObject.SetActive(true);
    Door.transform.rotation = Quaternion.Euler(0,260,0);
        Debug.Log("door open");
        AudioSource.PlayClipAtPoint(DoorOpen, Door.transform.position,1f);
    }
    }
    private void OnCollisionExit(Collision other) {
        if(other.gameObject.name == "Player")
        {Msg.gameObject.SetActive(false); 
    }
    }
    // Update is called once per frame
    void Update()
    {
        // Debug.Log("door open");
    }
}

